$(function(){
	// Получение данных
	$.ajax({
		type: "GET",
		url: "http://university.netology.ru/api/currency",
		dateType: "json",
		success: function(data) {
			$(data).each(function (i) {
				$('.currency').append('<option value="' + data[i].Value + '">' + data[i].Name + '</option>');
	    });

			$('#load').hide();
			$('.wrap_form').css('display', 'inline-block');
	  }
	});

	// Ограничение на ввод в input
	$('input').on("change keyup keypress input click", function(e) {
	    if ($(this).val().match(/[^0-9.]/g)) {
	        $(this).val($(this).val().replace(/[^0-9.]/g, ''));
	    }
			if ($(this).val().indexOf(".") != '-1') {
		    return !(/[.]/.test(String.fromCharCode(e.charCode)));
		  }
	});

	// Функция конвертации валют
	function converter(dataCurrency) {
		var initialCurrency = $("#initialCurrency :selected").val(),
				initialCurrencySum = $("#initialCurrencySum").val(),
				newCurrency = $("#newCurrency :selected").val(),
				newCurrencySum = $("#newCurrencySum").val();

		if(dataCurrency == 'initial') {
			sum = initialCurrency * initialCurrencySum / newCurrency;
			$('#newCurrencySum').val(sum.toFixed(4));
		} else if(dataCurrency == 'new') {
			sum = newCurrency * newCurrencySum / initialCurrency;
			$('#initialCurrencySum').val(sum.toFixed(4));
		}
	}
	
	// Вызовы функции конвертации
	$(".changeCur").on('change', function () {
		var dataCurrency = $(this).data('currency');
		converter(dataCurrency);
	});
});
